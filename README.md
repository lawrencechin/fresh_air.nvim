# Fresh_Air.nvim
![header image](./media/Fresh_Air_Header_2.png)

A **Neovim** theme written in *lua* based on the colour scheme "fresh air" from the app [Mou](http://25.io/mou/). Also included is a **Neovim** version of the **Textmate** theme [Subway Seoul](https://github.com/idleberg/Subway.tmTheme) together with **dark** and *light* variations for all themes. 

Includes themes for [Kitty](https://sw.kovidgoyal.net/kitty/).

For more themes check out [Arsenixc_Themes.nvim](https://gitlab.com/lawrencechin/arsenixc_themes.nvim)

## Included Themes

![All Themes Screenshot](./media/all_themes.png)

Theme Name | Code Name
-- | --
Fresh Air | `fresh_air`
Fresh Air Original | `fresh_air_original`
Crisp Night | `crisp_night`
Crisp Night Lighter | `crisp_air_lighter`
Subway Seoul | `subway_seoul`
Subway Seoul Dark | `subway_seoul_dark`

## Installation & Usage

> Requires Neovim >= 0.8.0

Install via a package manager:

``` lua
require "paq" {
    { url = "https://gitlab.com/lawrencechin/fresh_air.nvim" };
}
```

Enable the colour scheme:

``` vim
" Vimscript
colorscheme fresh_air
```

``` lua
-- Lua
require( "fresh_air" ).setup()
```

To select a specific theme:

``` vim
" Vimscript
" default value: fresh_air
let g:fresh_air_style = "$style"
```

``` lua
-- Lua
-- default value: fresh_air
vim.g.fresh air_style = "$style"
```

To enable a transparent background:

``` vim
" Vimscript
" default value: false
let g:fresh_air_transparent_background = true
```

``` lua
-- Lua
-- default value: false
vim.g.fresh_air_transparent_background = true
```

To cycle through themes:

``` vim
:lua require("fresh_air.functions").change_style("$style")
" Alternatively, use the following after loading the scheme to save undue typing
:FreshAir <TAB> $style
```

To switch themes based on the current system theme (**dark**/*light*) take a ganders at [Dark Notify](https://github.com/cormacrelf/dark-notify/)

## Credits

- [material.nvim](https://github.com/marko-cerovac/material.nvim) & [nebulous.nvim](https://github.com/Yagua/nebulous.nvim) used as a basis to write lua colour schemes. I also stole the comments for what each highlight group targets.
- [Treesitter Playground](https://github.com/nvim-treesitter/playground) proved very useful to check what highlight group were active for the text under the cursor (`:TSHighlightCapturesUnderCursor`).

![Thanks for viewing](./media/a3_75.gif)
