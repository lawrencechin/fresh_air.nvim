local function change_style( style )
    vim.g.fresh_air_style = style
    vim.cmd[[colorscheme fresh_air]]
end

local function list_themes()
    local themes = {
        "fresh_air",
        "fresh_air_original",
        "crisp_night",
        "crisp_night_lighter",
        "subway_seoul",
        "subway_seoul_dark"
    }

    return themes
end

vim.api.nvim_create_user_command( 
    "FreshAir", 
    function( opts )
        change_style( opts.args )
    end,
    {
        nargs = 1,
        complete = list_themes
    }
)

local M = {}

M.change_style = change_style
M.list_themes = list_themes

return M
