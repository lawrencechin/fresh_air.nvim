local Subway_Seoul_Dark = {}

-- Subway_Seoul_Dark.Bold_Italic = "#AFAFAF"
-- Subway_Seoul_Dark.Text_Colour = "#EFEFEF"
Subway_Seoul_Dark.Background_Colour = "#2F2B29"
Subway_Seoul_Dark.Black = "#303030"
Subway_Seoul_Dark.Blue = "#1CABDF"
Subway_Seoul_Dark.Bold_Italic = "#C7C5C5"
Subway_Seoul_Dark.Dark_Magenta = "#C964B2"
Subway_Seoul_Dark.Green = "#3E9F64"
Subway_Seoul_Dark.Grey = "#a0a0a0"
Subway_Seoul_Dark.Pink = "#E175A5"
Subway_Seoul_Dark.Popup_Colour = "#4D4642"
Subway_Seoul_Dark.Silver = "#c0c0c0"
Subway_Seoul_Dark.Steel_Blue = "#98C7F1"
Subway_Seoul_Dark.Taupe = "#B58E7B"
Subway_Seoul_Dark.Text_Colour = "#EBE8E8"
Subway_Seoul_Dark.White = "#ffffff"
Subway_Seoul_Dark.Yellow = "#ecaf40"

local match_fresh_air_name = {}

match_fresh_air_name.Background_Colour = Subway_Seoul_Dark.Background_Colour
match_fresh_air_name.Black = Subway_Seoul_Dark.Black
match_fresh_air_name.Blue = Subway_Seoul_Dark.Blue
match_fresh_air_name.Bold_Italic = Subway_Seoul_Dark.Bold_Italic
match_fresh_air_name.Green = Subway_Seoul_Dark.Green
match_fresh_air_name.Grey = Subway_Seoul_Dark.Silver
match_fresh_air_name.Magenta = Subway_Seoul_Dark.Yellow
match_fresh_air_name.Mauve = Subway_Seoul_Dark.Taupe
match_fresh_air_name.Orange = Subway_Seoul_Dark.Taupe
match_fresh_air_name.Pink = Subway_Seoul_Dark.Pink
match_fresh_air_name.Popup_Colour = Subway_Seoul_Dark.Popup_Colour
match_fresh_air_name.Purple = Subway_Seoul_Dark.Dark_Magenta
match_fresh_air_name.Red = Subway_Seoul_Dark.Pink
match_fresh_air_name.Slate_Grey = Subway_Seoul_Dark.Grey
match_fresh_air_name.Strong_Purple = Subway_Seoul_Dark.Steel_Blue
match_fresh_air_name.Text_Colour = Subway_Seoul_Dark.Text_Colour
match_fresh_air_name.White = Subway_Seoul_Dark.White
match_fresh_air_name.Yellow = Subway_Seoul_Dark.White


return match_fresh_air_name
