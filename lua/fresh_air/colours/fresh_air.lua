local Fresh_Air = {}

-- Fresh_Air.Bold_Italic = "#444444";
-- Fresh_Air.Popup_Colour = "#EEEEEE"
-- Fresh_Air.Text_Colour = "#212121"
Fresh_Air.Background_Colour = "#FFFFFF"
Fresh_Air.Black = "#000000"
Fresh_Air.Bold_Italic = "#5C4D78"
Fresh_Air.Blue = "#009cd9"
Fresh_Air.Green = "#00990f"
Fresh_Air.Grey = "#a2b0b0"
Fresh_Air.Magenta = "#e02da8"
Fresh_Air.Mauve = "#7c86ce"
Fresh_Air.Orange = "#dc6122"
Fresh_Air.Pink = "#ff6ca1" 
Fresh_Air.Popup_Colour = "#DDD8D8"
Fresh_Air.Purple = "#603da8"
Fresh_Air.Red = "#eb4b3f"
Fresh_Air.Slate_Grey = "#688088"
Fresh_Air.Strong_Purple = "#4e279a"
Fresh_Air.Text_Colour = "#251249"
Fresh_Air.White = "#feffff"
Fresh_Air.Yellow = "#eca440"

return Fresh_Air
