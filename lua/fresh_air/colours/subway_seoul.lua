local Subway_Seoul = {}

-- Colour names match actual colour
-- Exported object matches Fresh Air names
-- for scheme consistency

-- Subway_Seoul.Bold_Italic = "#535353"
-- Subway_Seoul.Text_Colour = "#232323"
Subway_Seoul.Background_Colour = "#FFFFFF"
Subway_Seoul.Black = "#303030"
Subway_Seoul.Blue = "#1cabdf"
Subway_Seoul.Bold_Italic = "#7A6E69"
Subway_Seoul.Dark_Magenta = "#962C7E"
Subway_Seoul.Green = "#16a04c"
Subway_Seoul.Grey = "#a0a0a0"
Subway_Seoul.Pink = "#e23f88"
Subway_Seoul.Popup_Colour = "#DDD8D8"
Subway_Seoul.Silver = "#c0c0c0"
Subway_Seoul.Steel_Blue = "#2e76b6"
Subway_Seoul.Taupe = "#B58E7B"
Subway_Seoul.Text_Colour = "#49423F"
Subway_Seoul.White = "#ffffff"
Subway_Seoul.Yellow = "#ecaf40"

local match_fresh_air_name = {}

match_fresh_air_name.Background_Colour = Subway_Seoul.Background_Colour
match_fresh_air_name.Black = Subway_Seoul.Black
match_fresh_air_name.Blue = Subway_Seoul.Blue
match_fresh_air_name.Bold_Italic = Subway_Seoul.Bold_Italic
match_fresh_air_name.Green = Subway_Seoul.Green
match_fresh_air_name.Grey = Subway_Seoul.Silver
match_fresh_air_name.Magenta = Subway_Seoul.Dark_Magenta
match_fresh_air_name.Mauve = Subway_Seoul.Taupe
match_fresh_air_name.Orange = Subway_Seoul.Taupe
match_fresh_air_name.Pink = Subway_Seoul.Pink
match_fresh_air_name.Popup_Colour = Subway_Seoul.Popup_Colour
match_fresh_air_name.Purple = Subway_Seoul.Dark_Magenta
match_fresh_air_name.Red = Subway_Seoul.Pink
match_fresh_air_name.Slate_Grey = Subway_Seoul.Grey
match_fresh_air_name.Strong_Purple = Subway_Seoul.Steel_Blue
match_fresh_air_name.Text_Colour = Subway_Seoul.Text_Colour
match_fresh_air_name.White = Subway_Seoul.White
match_fresh_air_name.Yellow = Subway_Seoul.Yellow


return match_fresh_air_name
