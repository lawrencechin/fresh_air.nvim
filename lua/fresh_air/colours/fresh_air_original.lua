local Fresh_Air_Original = {}

-- Fresh_Air_Original.Bold_Italic = "#444444";
-- Fresh_Air_Original.Text_Colour = "#000000"
Fresh_Air_Original.Background_Colour = "#FFFFFF"
Fresh_Air_Original.Bold_Italic = "#5C4D78"
Fresh_Air_Original.Black = "#000000"
Fresh_Air_Original.Blue = "#268BD2"
Fresh_Air_Original.Green = "#238C00"
Fresh_Air_Original.Grey = "#93A1A1"
Fresh_Air_Original.Magenta = "#CF019A"
Fresh_Air_Original.Mauve = "#6C71C4"
Fresh_Air_Original.Orange = "#CB4B16"
Fresh_Air_Original.Pink = "#F94F92"
Fresh_Air_Original.Popup_Colour = "#DDD8D8"
Fresh_Air_Original.Purple = "#4E289A"
Fresh_Air_Original.Red = "#DC322F"
Fresh_Air_Original.Slate_Grey = "#586E75"
Fresh_Air_Original.Strong_Purple = "#4E279A"
Fresh_Air_Original.Text_Colour = "#251249"
Fresh_Air_Original.White = "#feffff"
Fresh_Air_Original.Yellow = "#eca440"

return Fresh_Air_Original
