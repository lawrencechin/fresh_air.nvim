local Colours = {}
local variant = vim.g.fresh_air_style

---Variants selector
if variant == "crisp_night" then
    Colours = require( "fresh_air.colours.crisp_night" )
elseif variant == "crisp_night_lighter" then
    Colours = require( "fresh_air.colours.crisp_night_lighter" )
elseif variant == "subway_seoul" then
    Colours = require( "fresh_air.colours.subway_seoul" ) 
elseif variant == "subway_seoul_dark" then
    Colours = require( "fresh_air.colours.subway_seoul_dark" )
elseif variant == "fresh_air_original" then
    Colours = require( "fresh_air.colours.fresh_air_original" )
else
    Colours = require( "fresh_air.colours.fresh_air") --Default schema
end

Colours.none = "NONE"

return Colours
