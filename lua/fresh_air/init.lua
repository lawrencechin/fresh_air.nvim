local utils = require( "fresh_air.utils" )
local functions = require( "fresh_air.functions" )
local fresh_air = {}

function fresh_air.setup()
    utils.load_colorscheme()
end

return fresh_air
