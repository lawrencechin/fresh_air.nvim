lua << EOF

package.loaded[ 'fresh_air' ]        = nil
package.loaded[ 'fresh_air.utils' ]  = nil
package.loaded[ 'fresh_air.function' ] = nil
package.loaded[ 'fresh_air.colours' ] = nil
package.loaded[ 'fresh_air.scheme' ] = nil

require("fresh_air").setup()

EOF
